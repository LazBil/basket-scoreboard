function activeInactiveButtons(u) {
    if (u === 0) {
      for (let i = 0; i < allPlayers.length; ++i) {
        if (allPlayers[i].isActive === "out") {
          document.getElementsByClassName("foul-shot")[i].disabled = true;
          document.getElementsByClassName("normal-shot")[i].disabled = false;
          document.getElementsByClassName("long-shot")[i].disabled = false;
          document.getElementsByClassName("fouls")[i].disabled = false;
          document.getElementsByClassName("substitution")[i].disabled = true;
        } else {
          document.getElementsByClassName("fouls")[i].disabled = false;
          document.getElementsByClassName("substitution")[i].disabled = true;
        }
      }
    } else {
      for (let i = 0; i < allPlayers.length; ++i) {
        if (allPlayers[i].isActive === "out") {
          document.getElementsByClassName("foul-shot")[i].disabled = false;
          document.getElementsByClassName("normal-shot")[i].disabled = true;
          document.getElementsByClassName("long-shot")[i].disabled = true;
          document.getElementsByClassName("fouls")[i].disabled = false;
          document.getElementsByClassName("substitution")[i].disabled = false;
        } else {
          document.getElementsByClassName("fouls")[i].disabled = false;
          document.getElementsByClassName("substitution")[i].disabled = false;
        }
      }
    }
  }