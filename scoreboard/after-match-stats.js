function addScore() {
  if (side === "home") {
    presentTeamName[1].score = guest_score;
  } else {
    presentTeamName[0].score = home_score;
  }
  console.log(presentTeamName);
}

function endGame() {
  if (current === 0) {
    localStorage.setItem("homeTeam", JSON.stringify(presentHomePlayers));
    localStorage.setItem("guestTeam", JSON.stringify(presentGuestPlayers));
    localStorage.setItem("TeamsScore", JSON.stringify(presentTeamName));
  }
}
