//function calling other functions
(function showTeams(){
  for (let i = 0; i < presentTeamName.length; ++i) {
    document.getElementById("homeTeamName").innerHTML =
      presentTeamName[0].teamName;
    document.getElementById("guestTeamName").innerHTML =
      presentTeamName[1].teamName;
  }
})();

function homePossesion() {
  stopTimer();
  stopPeriodTimer();
  offstop();
  s = 24;
  activeInactiveButtons(u);
}

function guestPossesion() {
  offstop();
  stopTimer();
  stopPeriodTimer();
  s = 24;
  activeInactiveButtons(u);
}

function gameStart() {
  startTimer();
  startPeriodTimer();
  printNumbers(s);
}

function gameStop() {
  stopTimer();
  stopPeriodTimer();
  clearTimeout(z);
}

function startStop() {
  var inCourt = allActivePlayers();
  if (u === 0) {
    if (inCourt !== 10){
      alert("one team has less than 5 players")
      return;
    }else {
      activeInactiveButtons(u);
      gameStart();
    }
  } else {
    activeInactiveButtons(u);
    gameStop();
  }
}

function gameReset() {
  for (let i = 0; i < allPlayers.length; ++i) {
    document.getElementsByClassName("foul-shot")[i].disabled = true;
    document.getElementsByClassName("normal-shot")[i].disabled = true;
    document.getElementsByClassName("long-shot")[i].disabled = true;
    document.getElementsByClassName("fouls")[i].disabled = true;
    document.getElementsByClassName("substitution")[i].disabled = true;
  }
  stopTimer();
  stopPeriodTimer();
  resetPeriodTimer();
  resetTimer();
  periodStop();
  reset();
  zeroall();
  s = 24;
  side = "home";
  periodNumber.innerHTML = "Period # " + l;
}

function zeroall() {
  homeOffenseTimer.innerHTML = "00";
  guestOffenseTimer.innerHTML = "00";
}

window.addEventListener("keydown", function(e) {
  switch (e.which || e.keyCode) {
    case 49:
      if (u === 1) {
        return;
      } else {
        points(1);
        addScore();
        activeInactiveButtons(u);
      }
      break;
    case 50:
      if (u === 0) {
        return;
      } else {
        activeInactiveButtons(u);
        points(2);
        addScore();
      }
      break;
    case 51:
      if (u === 0) {
        return;
      } else {
        activeInactiveButtons(u);
        points(3);
        addScore();
      }
      break;
    case 32:
      startStop();
      break;
    case 27:
      gameReset();
      break;
    case 71:
      if (side === "home") {
        guestPossesion();
      }
      break;
    case 72:
      if (side === "guest") {
        homePossesion();
      }
      break;
  }
});

document.getElementById("swap-sides").addEventListener("click", change_sides);
document.getElementById("start-stop-btn").addEventListener("click", startStop);
document.getElementById("game-reset-btn").addEventListener("click", gameReset);
