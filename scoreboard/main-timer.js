const timerElement = document.getElementById("timer");

function renderTimer() {
  timerElement.innerHTML = transformTimer(current--);
}

function transformTimer(currentSeconds) {
  const minutes = String(Math.floor(currentSeconds / 60));
  const seconds = String(currentSeconds % 60);
  if (minutes === "0" && seconds === "0") {
    stopTimer();
  }

  return minutes.padStart(2, 0) + ":" + seconds.padStart(2, 0);
}

function startTimer() {
  u = 1;
  timer = setInterval(function() {
    renderTimer();
  }, 1000);
}

function stopTimer() {
  u = 0;
  clearInterval(timer);
  endGame();
}

function resetTimer() {
  current = MATCH_START;
  renderTimer();
}

renderTimer();
