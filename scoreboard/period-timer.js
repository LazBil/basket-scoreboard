periodNumber.innerHTML = "Period #" + l;

const periodTimerElement = document.getElementById("period-countdown");

function renderPeriodTimer() {
  periodTimerElement.innerHTML = transformPeriodTimer(current_period--);
}

function transformPeriodTimer(currentPeriodSeconds) {
  const periodMinutes = String(Math.floor(currentPeriodSeconds / 60));
  const periodSeconds = String(currentPeriodSeconds % 60);
  if (periodMinutes === "0" && periodSeconds === "0" && l <= 4) {
    stopTimer();
    stopPeriodTimer();
    resetPeriodTimer();
    periodStop();
    zeroall();
    periodStop();
    s = 24;
    l++;
    periodNumber.innerHTML = "Period # " + l;
  }
  if (periodMinutes == 0 && periodSeconds == 0 && l > 4) {
    stopPeriodTimer();
    periodNumber.innerHTML = "0";
  }
  return periodMinutes.padStart(2, 0) + ":" + periodSeconds.padStart(2, 0);
}

function startPeriodTimer() {
  u = 1;
  period_countdown = setInterval(function() {
    renderPeriodTimer();
  }, 1000);
}

function stopPeriodTimer() {
  u = 0;
  clearInterval(period_countdown);
}

function resetPeriodTimer() {
  current_period = PERIOD_START;
  renderPeriodTimer();
}

renderPeriodTimer();
