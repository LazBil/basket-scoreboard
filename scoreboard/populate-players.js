function populateHColumn() {
    //isos xreiastei na spaso paiktes kai buttons se duo divs
    var homeTeam = allHPlayers();
    for (let i = 0; i < homeTeam.length; ++i) {
      childElement = document.createElement("div");
      childElement.className = "homePlayers";
      childElement.style.padding = " 5px";
      appendChildElement = parentElement.appendChild(childElement);
      appendChildElement.innerText =
        homeTeam[i].name +
        " " +
        homeTeam[i].surname +
        " " +
        homeTeam[i].playerJersey +
        " " +
        homeTeam[i].isActive +
        " player fouls " +
        homeTeam[i].fouls;
      addbuttons(homeTeam[i]);
    }
    childElement = document.createElement("div");
    appendChildElement = parentElement.appendChild(childElement);
    appendChildElement.innerText = presentCoaches[0].coachName + " " + presentCoaches[0].coachSurname;
  }
  
  populateHColumn();
  
  function populateGColumn() {
    var guestTeam = allGPlayers;
    for (let i = 0; i < guestTeam.length; ++i) {
      childElement = document.createElement("div");
      childElement.className = "guestPlayers";
      appendChildElement = parentElement2.appendChild(childElement);
      appendChildElement.innerHTML =
        guestTeam[i].name +
        " " +
        guestTeam[i].surname +
        " " +
        guestTeam[i].playerJersey +
        " " +
        guestTeam[i].isActive +
        " player fouls " +
        guestTeam[i].fouls;
      addbuttons(guestTeam[i]);
    }
    childElement = document.createElement("div");
    appendChildElement = parentElement2.appendChild(childElement);
    appendChildElement.innerText = presentCoaches[1].coachName + " " + presentCoaches[1].coachSurname;
  }
  
  populateGColumn();

  function reRender(){
    while (parentElement.firstChild) {
      parentElement.removeChild(parentElement.firstChild);
    }
    while (parentElement2.firstChild) {
      parentElement2.removeChild(parentElement2.firstChild);
    }
    populateHColumn();
    populateGColumn();
  }

  function addbuttons(player) {
    let shot = document.createElement("button");
    shot.className = "foul-shot";
    shot.innerText = "+1";
    shot.disabled = true;
    shot.addEventListener("click", passPoints);
    shot.addEventListener("click", addScore);
    shot.addEventListener("click", () => {
      player.points += 1;
      console.log(
        "player with number " + player.playerJersey + " " + player.points
      );
    });
    let inzone = document.createElement("button");
    inzone.className = "normal-shot";
    inzone.disabled = true;
    inzone.innerText = "+2";
    inzone.addEventListener("click", passPoints);
    inzone.addEventListener("click", addScore);
    inzone.addEventListener("click", () => {
      player.points += 2;
      console.log(
        "player with number " + player.playerJersey + " " + player.points
      );
    });
    let longshot = document.createElement("button");
    longshot.className = "long-shot";
    longshot.disabled = true;
    longshot.innerText = "+3";
    longshot.addEventListener("click", passPoints);
    longshot.addEventListener("click", addScore);
    longshot.addEventListener("click", () => {
      player.points += 3;
      console.log(
        "player with number " + player.playerJersey + " " + player.points
      );
    });  
    let foulPoint = document.createElement("button");
    foulPoint.className = "fouls";
    foulPoint.innerText = "foul";
    foulPoint.disabled = true;
    foulPoint.addEventListener("click", () => {
      if (player.fouls < 4) {
        player.fouls += 1;
        gameStop();
        change_sides();
        console.log(parentElement);
        reRender();
        activeInactiveButtons();
      } else {
        player.fouls += 1;
        shot.disabled = true;
        inzone.disabled = true;
        longshot.disabled = true;
        foulPoint.disabled = true;
        sub.disabled = true;
        player.isActive = "in";
        alert("the player with the number " + player.playerJersey + " is out");
        reRender();
      }
    });
    let sub = document.createElement("button");
    sub.className = "substitution";
    sub.innerText = "Sub";
    sub.addEventListener("click", () => {
      if (player.isActive === "out") {
        player.isActive = "in";
      } else {
        player.isActive = "out";
      }
      reRender();
      activeInactiveButtons();
      console.log(player);
      console.log(player.isActive);
    });
    childElement.appendChild(shot);
    childElement.appendChild(inzone);
    childElement.appendChild(longshot);
    childElement.appendChild(foulPoint);
    childElement.appendChild(sub);
  }