//checks number of points added
function points(point) {
  if (side === "home") {
    if (point === 1 && tries < 3) {
      homeTeamScore.innerHTML = home_score += point;
      tries += 1;
    } else {
      homeTeamScore.innerHTML = home_score += point;
      stopTimer();
      stopPeriodTimer();
      offstop();
      s = 24;
      tries = 1;
      return;
    }
  }
  if (side === "guest") {
    if (point === 1 && tries < 3) {
      guestTeamScore.innerHTML = guest_score += point;
      tries += 1;
    } else {
      guestTeamScore.innerHTML = guest_score += point;
      stopTimer();
      stopPeriodTimer();
      offstop();
      s = 24;
      tries = 1;
      return;
    }
  }
}

//gives the parameter to points and calls it
function passPoints() {
    event.srcElement.innerText === "+1" ? points(1) : null;
    event.srcElement.innerText === "+2" ? points(2) : null;
    event.srcElement.innerText === "+3" ? points(3) : null;
}

//resets scoreboard

function reset() {
  homeTeamScore.innerHTML = home_score - home_score;
  guestTeamScore.innerHTML = guest_score - guest_score;
}
