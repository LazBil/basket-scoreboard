var homeOffenseTimer = document.getElementById("home-offense-timer");
var guestOffenseTimer = document.getElementById("guest-offense-timer");
var homeTeamScore = document.getElementById("home-team__score");
var guestTeamScore = document.getElementById("guest-team__score");
var periodNumber = document.getElementById("period-number");

//variables for the two main divs that render the team columns
parentElement = document.getElementById("home-column");
parentElement2 = document.getElementById("guest-column");

//variables for change team and score
var side;
var home_score = 0;
var guest_score = 0;
var tries = 1;
//variables for match timer
const MATCH_START = 40 * 60;
var current = MATCH_START;
var timer;
var u = 0;
//variables for period timer
var l = 1;
const PERIOD_START = 10 * 60;
var current_period = PERIOD_START;
var period_countdown;
//variables for offense countdown
var z = 0;
var s = 24;

// retrieves the saved object arrays
var homeSavedTeam = localStorage.getItem("homeTeamPlayers");
var guestSavedTeam = localStorage.getItem("guestTeamPlayers");
var savedTeams = localStorage.getItem("teams");
var savedCoaches = localStorage.getItem("coaches");

//parses stringified objects from localstorage
var presentTeamName = JSON.parse(savedTeams);
var presentCoaches = JSON.parse(savedCoaches);
var presentHomePlayers = JSON.parse(homeSavedTeam);
var presentGuestPlayers = JSON.parse(guestSavedTeam);
var allPlayers = presentHomePlayers.concat(presentGuestPlayers);

function allHPlayers() {
  var hActivePlayers = presentHomePlayers.filter(function(player) {
    return player.isActive === "out";
  });
  var hInactivePlayers = presentHomePlayers.filter(function(player) {
    return player.isActive === "in";
  });
  var hPlayers = hActivePlayers.concat(hInactivePlayers);
  console.log(hPlayers);
  return hPlayers;
}

function allGPlayers(){
    var gActivePlayers = presentGuestPlayers.filter(function(player) {
        return player.isActive === "out";
      });
      var gInactivePlayers = presentGuestPlayers.filter(function(player) {
        return player.isActive === "in";
      });
    var gPlayers = gActivePlayers.concat(gInactivePlayers);
    return gPlayers;
}

function allActivePlayers (){
  var hActivePlayers = presentHomePlayers.filter(function(player) {
    return player.isActive === "out";
  });
  var gActivePlayers = presentGuestPlayers.filter(function(player) {
    return player.isActive === "out";
  });
  var activePlayers = hActivePlayers.concat(gActivePlayers);
  return activePlayers; 
}