function clearPlayer(team) {
  if (team === "home") {
    document.querySelector(".hPlayer-name").value = "";
    document.querySelector(".hPlayer-surname").value = "";
    document.querySelector(".hJersey").value = "";
  } else {
    document.querySelector(".gPlayer-name").value = "";
    document.querySelector(".gPlayer-surname").value = "";
    document.querySelector(".gJersey").value = "";
  }
}

function clearCoach() {
  var clearingName = document.getElementsByClassName("coach-name");
  var clearingSurname = document.getElementsByClassName("coach-surname");
  for (let i = 0; i < clearingName.length; ++i) {
    clearingName[i].value = "";
    clearingSurname[i].value = "";
  }
  document.querySelector(".coaches-button").disabled = true;
}

function clearTeam() {
  var clearing = document.getElementsByClassName("team-name");
  for (let i = 0; i < clearing.length; ++i) {
    clearing[i].value = "";
  }
  document.querySelector(".teams-button").disabled = true;
}