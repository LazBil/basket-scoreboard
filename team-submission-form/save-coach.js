// import {clearCoach} from "./clears";

function saveCoach() {
  const coachName = document.getElementsByClassName("coach-name");
  const coachSurname = document.getElementsByClassName("coach-surname");
  if (
    document.querySelector(".coach-name").value === "" ||
    document.querySelector(".coach-surname").value === ""
  ) {
    alert("Please input all fields for the coaches");
    return;
  } else {
    for (let i = 0; i < coachName.length; ++i) {
      var _coach = new coach(
        coachName[i].value,
        coachSurname[i].value,
        "coach"
      );
      coaches.push(_coach);
    }
    clearCoach();
    disabledSubmit();
    console.log(coaches);
  }
}

function clearCoach() {
  var clearingName = document.getElementsByClassName("coach-name");
  var clearingSurname = document.getElementsByClassName("coach-surname");
  for (let i = 0; i < clearingName.length; ++i) {
    clearingName[i].value = "";
    clearingSurname[i].value = "";
  }
  document.querySelector(".coaches-button").disabled = true;
}
