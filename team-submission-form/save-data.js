function startGame() {
  localStorage.setItem("homeTeamPlayers", JSON.stringify(homeTeamPlayers));
  localStorage.setItem("guestTeamPlayers", JSON.stringify(guestTeamPlayers));
  localStorage.setItem("teams", JSON.stringify(teams));
  localStorage.setItem("coaches", JSON.stringify(coaches));
}
