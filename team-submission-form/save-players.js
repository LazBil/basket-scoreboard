// import {clearPlayer} from "./clears";

function saveHPlayer() {
  const hPlayerName = document.querySelector(".hPlayer-name");
  const hPlayerSurname = document.querySelector(".hPlayer-surname");
  const hPlayerJersey = document.querySelector(".hJersey");
  if (
    hPlayerName.value === "" ||
    hPlayerSurname.value === "" ||
    hPlayerJersey.value === "" ||
    (homeRadio[0].checked === false && homeRadio[1].checked === false)
  ) {
    alert("Please add all fields for the home player");
    return;
  } else 
  if (homePlayerQuantity < 12) {
    player = new basketPlayer(
      hPlayerName.value,
      hPlayerSurname.value,
      hPlayerJersey.value,
      "player",
      0,
      0,
      "00:00"
    );
    playerActivity(player, homeRadio);
    if (
      homeTeamPlayers.some(
        person => person.playerJersey === player.playerJersey
      )
    ) {
      alert("check your inputs");
    } else {
      homeTeamPlayers.push(player);
      disableRadio("home");
      clearPlayer("home");
      ++homePlayerQuantity;
    }
    console.log(homeTeamPlayers);
  }
  if (homePlayerQuantity === 12) {
    document.getElementById("homeTeamPlayer").disabled = true;
  }
  disabledSubmit();
}

function saveGPlayer() {
  const gPlayerName = document.querySelector(".gPlayer-name");
  const gPlayerSurname = document.querySelector(".gPlayer-surname");
  const gPlayerJersey = document.querySelector(".gJersey");
  if (
    gPlayerName.value === "" ||
    gPlayerSurname.value === "" ||
    gPlayerJersey.value === "" ||
    (guestRadio[0].checked === false && guestRadio[1].checked === false)
  ) {
    alert("Please add all fields for the guest player");
    return;
  } else 
  if (guestPlayerQuantity < 12) {
    player = new basketPlayer(
      gPlayerName.value,
      gPlayerSurname.value,
      gPlayerJersey.value,
      "player",
      0,
      0,
      "00:00"
    );
    playerActivity(player, guestRadio);
    if (
      guestTeamPlayers.some(
        person => person.playerJersey === player.playerJersey
      )
    ) {
      alert("check your inputs");
    } else {
      guestTeamPlayers.push(player);
      disableRadio("guest");
      clearPlayer("guest");
      ++guestPlayerQuantity;
    }
    console.log(guestTeamPlayers);
  }
  if (guestPlayerQuantity === 12) {
    document.getElementById("guestTeamPlayer").disabled = true;
  }
  disabledSubmit();
}

function playerActivity(playerConst, radios) {
  for (let i = 0; i < radios.length; ++i) {
    if (radios[0].checked) {
      playerConst.isActive = "out";
    } else {
      playerConst.isActive = "in";
    }
  }
}

function clearPlayer(team) {
  if (team === "home") {
    document.querySelector(".hPlayer-name").value = "";
    document.querySelector(".hPlayer-surname").value = "";
    document.querySelector(".hJersey").value = "";
  } else {
    document.querySelector(".gPlayer-name").value = "";
    document.querySelector(".gPlayer-surname").value = "";
    document.querySelector(".gJersey").value = "";
  }
}

function disableRadio(team) {
  if (team === "home") {
    var activePlayers = homeTeamPlayers.filter(function(active) {
      return active.isActive === "out";
    });
    var inactivePlayers = homeTeamPlayers.filter(function(inactive) {
      return inactive.isActive === "in";
    });
    if (activePlayers.length === 5) {
      homeRadio[0].disabled = true;
      homeRadio[1].checked = true;
    }
    if (inactivePlayers.length === 7) {
      homeRadio[0].checked = true;
      homeRadio[1].disabled = true;
    }
  } else {
    var activePlayers = guestTeamPlayers.filter(function(active) {
      return active.isActive === "out";
    });
    var inactivePlayers = guestTeamPlayers.filter(function(inactive) {
      return inactive.isActive === "in";
    });
    if (activePlayers.length === 5) {
      guestRadio[0].disabled = true;
      guestRadio[1].checked = true;
    }
    if (inactivePlayers.length === 7) {
      guestRadio[0].checked = true;
      guestRadio[1].disabled = true;
    }
  }
}
