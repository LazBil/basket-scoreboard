function saveTeams() {
  const nameOfTeam = document.getElementsByClassName("team-name");
  if (document.querySelector(".team-name").value === "") {
    alert("Please add both team names");
    return;
  } else {
    for (let i = 0; i < nameOfTeam.length; ++i) {
      var _team = new team(nameOfTeam[i].value);
      teams.push(_team);
    }
    clearTeam();
    disabledSubmit();
  }
}

function clearTeam() {
  var clearing = document.getElementsByClassName("team-name");
  for (let i = 0; i < clearing.length; ++i) {
    clearing[i].value = "";
  }
  document.querySelector(".teams-button").disabled = true;
}
