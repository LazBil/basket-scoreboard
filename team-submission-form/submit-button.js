function disabledSubmit() {
  if (
    !teams.length ||
    !coaches.length ||
    homeTeamPlayers.length < 12 ||
    guestTeamPlayers.length < 12
  ) {
    document.getElementById("submitButton").disabled = true;
  } else {
    document.getElementById("submitButton").disabled = false;
  }
}

disabledSubmit();
