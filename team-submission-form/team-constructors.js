class team {
    constructor(id) {
      this.teamName = id;
    }
  }
  
  class coach {
    constructor(name, surname, role) {
      this.coachName = name;
      this.coachSurname = surname;
      this.role = role;
    }
  }
  
  class basketPlayer {
    constructor(name, surname, jersey, role, fouls, points, playTime) {
      this.name = name;
      this.surname = surname;
      this.playerJersey = jersey;
      this.role = role;
      this.fouls = fouls;
      this.points = points;
      this.playTime = playTime;
    }
  }