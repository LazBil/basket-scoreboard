document.querySelector(".teams-button").addEventListener("click", saveTeams);
document.querySelector(".coaches-button").addEventListener("click", saveCoach);
document
  .getElementById("homeTeamPlayer")
  .addEventListener("click", saveHPlayer);
document
  .getElementById("guestTeamPlayer")
  .addEventListener("click", saveGPlayer);
document.getElementById("submitButton").addEventListener("click", startGame);